package service

import (
	"go.octolab.org/pointer"
	"reflect"
	"test-findhotel/internal/geolocation"
	"testing"
)

func Test_parseGeoData(t *testing.T) {
	tests := []struct {
		name    string
		data    []string
		geo     *geolocation.GeoData
		wantErr bool
	}{
		{
			"Simple test",
			[]string{"111.52.81.88", "ET", "Anguilla", "East Clair", "-77.01809282517468", "123.08354510644665", "2193081396"},
			&geolocation.GeoData{
				IpAddress:    "111.52.81.88",
				CountryCode:  "ET",
				Country:      "Anguilla",
				City:         "East Clair",
				Latitude:     pointer.ToFloat64(-77.01809282517468),
				Longitude:    pointer.ToFloat64(123.08354510644665),
				MysteryValue: pointer.ToInt64(2193081396),
			},
			false,
		},
		{
			"Partial data",
			[]string{"111.52.81.88", "ET", "Anguilla", "East Clair", "", "", ""},
			&geolocation.GeoData{
				IpAddress:    "111.52.81.88",
				CountryCode:  "ET",
				Country:      "Anguilla",
				City:         "East Clair",
				Latitude:     nil,
				Longitude:    nil,
				MysteryValue: nil,
			},
			false,
		},
		{
			"Malformed data",
			[]string{"Hello", "my", "name", "is", "Anton", "Nice", "to", "meet", "you"},
			nil,
			true,
		},
		{
			"Invalid IP",
			[]string{"Hi there", "ET", "Anguilla", "East Clair", "-77.01809282517468", "123.08354510644665", "2193081396"},
			nil,
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseGeoData(tt.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseGeoData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.geo) {
				t.Errorf("parseGeoData() got = %v, want %v", got, tt.geo)
			}
		})
	}
}
