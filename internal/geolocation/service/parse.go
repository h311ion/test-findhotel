package service

import (
	"errors"
	"net"
	"strconv"
	"test-findhotel/internal/geolocation"
)

const (
	positionIp = iota
	positionCountryCode
	positionCountry
	positionCity
	positionLatitude
	positionLongitude
	positionMysteryValue

	numOfPositions = 7
)

var (
	errMalformedData = errors.New("malformed data")
	errInvalidIpv4   = errors.New("not a valid IPv4 address")
)

func parseGeoData(data []string) (g *geolocation.GeoData, err error) {
	if len(data) != numOfPositions {
		return nil, errMalformedData
	}

	ip := data[positionIp]
	if net.ParseIP(ip) == nil {
		return nil, errInvalidIpv4
	}
	g = &geolocation.GeoData{
		IpAddress:   ip,
		CountryCode: data[positionCountryCode],
		Country:     data[positionCountry],
		City:        data[positionCity],
	}
	lat, err := strconv.ParseFloat(data[positionLatitude], 64)
	if err == nil {
		g.Latitude = &lat
	}
	lon, err := strconv.ParseFloat(data[positionLongitude], 64)
	if err == nil {
		g.Longitude = &lon
	}
	m, err := strconv.ParseInt(data[positionMysteryValue], 10, 64)
	if err == nil {
		g.MysteryValue = &m
	}
	return g, nil
}
