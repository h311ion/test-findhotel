package service

import (
	"bytes"
	"github.com/golang/mock/gomock"
	"go.octolab.org/pointer"
	"reflect"
	"test-findhotel/internal/geolocation"
	"test-findhotel/internal/geolocation/storage"
	"testing"
)

func Test_service_GetByIpv4(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	var geo1 = geolocation.GeoData{
		IpAddress:    "127.0.0.1",
		CountryCode:  "RU",
		Country:      "Russia",
		City:         "Dolgoprudny",
		Latitude:     pointer.ToFloat64(123.456789),
		Longitude:    pointer.ToFloat64(-90.65332),
		MysteryValue: pointer.ToInt64(123321),
	}

	m := storage.NewMockKeyValue(ctrl)
	m.EXPECT().
		Get(gomock.Eq("127.0.0.1")).
		Return(&geo1, nil).
		AnyTimes()

	m.EXPECT().
		Get(gomock.Any()).
		Return(nil, storage.ErrorNotFound).
		AnyTimes()

	tests := []struct {
		name    string
		key     string
		want    *geolocation.GeoData
		wantErr bool
	}{
		{
			"simple test",
			"127.0.0.1",
			&geo1,
			false,
		},
		{
			"wrong request",
			"192.168.0.1",
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := New(m)
			got, err := s.GetByIpv4(tt.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByIpv4() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByIpv4() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_service_Load(t *testing.T) {
	tests := []struct {
		name         string
		storedValues []geolocation.GeoData
		csvData      []byte
		want         *geolocation.Response
		wantErr      bool
	}{
		{
			name: "greeen path",
			storedValues: []geolocation.GeoData{
				{
					"28.35.67.139",
					"VA",
					"Ukraine",
					"West Saige",
					pointer.ToFloat64(-40.522507308779765),
					pointer.ToFloat64(145.21786239931606),
					pointer.ToInt64(1681367080),
				},
			},
			csvData: []byte("ip_address,country_code,country,city,latitude,longitude,mystery_value\n" +
				"28.35.67.139,VA,Ukraine,West Saige,-40.522507308779765,145.21786239931606,1681367080\n" +
				",GT,Saudi Arabia,,42.199095608893316,-30.498288149973206,0"),
			want: &geolocation.Response{
				NumberOfErrors: 1,
				NumberOfOk:     1,
			},
		},
		{
			name:         "empty input",
			storedValues: nil,
			csvData:      nil,
			want: &geolocation.Response{
				NumberOfErrors: 0,
				NumberOfOk:     0,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockStorage := storage.NewMockKeyValue(ctrl)
			for _, value := range tt.storedValues {
				mockStorage.EXPECT().
					Add(gomock.Eq(value)).
					Return(nil).
					Times(1)
			}

			s := New(mockStorage)

			r := bytes.NewReader(tt.csvData)

			got, err := s.Load(r)

			if (err != nil) != tt.wantErr {
				t.Errorf("Load() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Load() got = %v, want %v", got, tt.want)
			}
		})
	}
}
