package service

import (
	"encoding/csv"
	"io"
	"test-findhotel/internal/geolocation"
	"test-findhotel/internal/geolocation/storage"
)

type service struct {
	store storage.KeyValue
}

func New(s storage.KeyValue) geolocation.DataServer {
	return &service{
		store: s,
	}
}

func (s *service) GetByIpv4(ip string) (*geolocation.GeoData, error) {
	return s.store.Get(ip)
}

func (s *service) Load(r io.Reader) (*geolocation.Response, error) {
	resp := geolocation.Response{}

	csvReader := csv.NewReader(r)
	isFirst := true

	for {
		data, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if isFirst {
			isFirst = false
			continue
		}
		d, err := parseGeoData(data)
		if err != nil || d == nil {
			resp.NumberOfErrors++
			continue
		}

		err = s.store.Add(*d)
		if err != nil {
			return nil, err
		}
		resp.NumberOfOk++
	}

	return &resp, nil
}
