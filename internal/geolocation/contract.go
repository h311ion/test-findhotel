package geolocation

import (
	"io"
)

//go:generate mockgen -package $GOPACKAGE -destination contract_mock.go test-findhotel/internal/geolocation DataServer

type Response struct {
	NumberOfErrors uint
	NumberOfOk     uint
}

type GeoData struct {
	IpAddress    string   `json:"ipv4"`
	CountryCode  string   `json:"countryCode"`
	Country      string   `json:"country"`
	City         string   `json:"city"`
	Latitude     *float64 `json:"latitude"`
	Longitude    *float64 `json:"longitude"`
	MysteryValue *int64   `json:"mysteryValue"`
}

type DataServer interface {
	Load(r io.Reader) (*Response, error)
	GetByIpv4(ip string) (*GeoData, error)
}
