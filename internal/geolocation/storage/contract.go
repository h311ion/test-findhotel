package storage

import (
	"errors"
	"test-findhotel/internal/geolocation"
)

//go:generate mockgen -package $GOPACKAGE -destination contract_mock.go test-findhotel/internal/geolocation/storage KeyValue

var ErrorNotFound = errors.New("data not found by key")

type KeyValue interface {
	// Get value by key, returns error on problems with storage
	Get(key string) (*geolocation.GeoData, error)
	// Add some value to storage, returns error on problems with storage
	Add(data geolocation.GeoData) error
}
