package storage

import (
	"test-findhotel/internal/geolocation"
)

type memoryStorage struct {
	data map[string]*geolocation.GeoData
}

//NewMemoryStorage simplest in-memory storage
func NewMemoryStorage(data []geolocation.GeoData) KeyValue {
	m := make(map[string]*geolocation.GeoData, len(data))
	for i := range data {
		m[data[i].IpAddress] = &data[i]
	}
	return &memoryStorage{
		data: m,
	}
}

func (s memoryStorage) Get(key string) (*geolocation.GeoData, error) {
	data, ok := s.data[key]
	if !ok {
		return nil, ErrorNotFound
	}

	return data, nil
}

func (s memoryStorage) Add(data geolocation.GeoData) error {
	s.data[data.IpAddress] = &data

	return nil
}
