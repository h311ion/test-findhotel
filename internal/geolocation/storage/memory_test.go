package storage

import (
	"errors"
	"go.octolab.org/pointer"
	"reflect"
	"test-findhotel/internal/geolocation"
	"testing"
)

var (
	geo1 = geolocation.GeoData{
		IpAddress:    "127.0.0.1",
		CountryCode:  "RU",
		Country:      "Russia",
		City:         "Dolgoprudny",
		Latitude:     pointer.ToFloat64(123.456789),
		Longitude:    pointer.ToFloat64(-90.65332),
		MysteryValue: pointer.ToInt64(123321),
	}
	geo2 = geolocation.GeoData{
		IpAddress:    "192.168.0.1",
		CountryCode:  "NL",
		Country:      "Netherlands",
		City:         "Dolgoprudny",
		Latitude:     pointer.ToFloat64(23.314535),
		Longitude:    pointer.ToFloat64(0.89877),
		MysteryValue: pointer.ToInt64(999888),
	}
	geo3 = geolocation.GeoData{
		IpAddress:    "10.0.0.1",
		CountryCode:  "US",
		Country:      "United States",
		City:         "Memphis",
		Latitude:     nil,
		Longitude:    nil,
		MysteryValue: nil,
	}
)

func Test_memoryStorage(t *testing.T) {
	type toFindStruct struct {
		geo      geolocation.GeoData
		wantErr  bool
		wantFind bool
	}
	type toAddStruct struct {
		geo     geolocation.GeoData
		wantErr bool
	}

	tests := []struct {
		name       string
		sampleData []geolocation.GeoData
		toAdd      []toAddStruct
		toFind     []toFindStruct
	}{
		{
			"Empty test",
			nil,
			nil,
			nil,
		},
		{
			name:       "Simple find test",
			sampleData: []geolocation.GeoData{geo1, geo3},
			toAdd:      nil,
			toFind: []toFindStruct{
				{geo1, false, true},
				{geo2, false, false},
				{geo3, false, true},
			},
		},
		{
			name:       "Add test",
			sampleData: []geolocation.GeoData{geo1},
			toAdd: []toAddStruct{
				{geo2, false},
			},
			toFind: []toFindStruct{
				{geo1, false, true},
				{geo2, false, true},
				{geo3, false, false},
			},
		},
		{
			name:       "Multiple add test",
			sampleData: []geolocation.GeoData{geo1},
			toAdd: []toAddStruct{
				{geo2, false},
				{geo2, false},
				{geo2, false},
				{geo2, false},
			},
			toFind: []toFindStruct{
				{geo1, false, true},
				{geo2, false, true},
				{geo3, false, false},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewMemoryStorage(tt.sampleData)
			for _, add := range tt.toAdd {
				err := s.Add(add.geo)
				if (err != nil) != add.wantErr {
					t.Errorf("Add() error = %v, wantErr %v", err, add.wantErr)
				}
			}

			for _, find := range tt.toFind {
				geo, err := s.Get(find.geo.IpAddress)
				isFound := geo != nil
				if isFound != find.wantFind {
					t.Error("Get() returned not found error, expected otherwise")
				}
				if (err != nil) != find.wantErr && !errors.Is(err, ErrorNotFound) {
					t.Errorf("Add() error = %v, wantErr %v", err, find.wantErr)
				}
				if isFound && !reflect.DeepEqual(geo, &find.geo) {
					t.Errorf("Get() got = %v, want %v", geo, find)
				}
			}
		})
	}
}
