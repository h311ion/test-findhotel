package get_geodata

import (
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus/hooks/test"
	"go.octolab.org/pointer"
	"net/http"
	"net/http/httptest"
	"test-findhotel/internal/geolocation"
	"test-findhotel/internal/geolocation/storage"
	"testing"
)

func Test_handler_GetGeodataFunc(t *testing.T) {
	var geo1 = geolocation.GeoData{
		IpAddress:    "127.0.0.1",
		CountryCode:  "RU",
		Country:      "Russia",
		City:         "Dolgoprudny",
		Latitude:     pointer.ToFloat64(123.456789),
		Longitude:    pointer.ToFloat64(-90.65332),
		MysteryValue: pointer.ToInt64(123321),
	}

	logger, _ := test.NewNullLogger()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	d := geolocation.NewMockDataServer(ctrl)
	d.EXPECT().
		GetByIpv4(gomock.Eq("127.0.0.1")).
		Return(&geo1, nil).
		AnyTimes()
	d.EXPECT().
		GetByIpv4(gomock.Eq("13.0.0.1")).
		Return(nil, errors.New("some internal error")).
		AnyTimes()
	d.EXPECT().
		GetByIpv4(gomock.Any()).
		Return(nil, storage.ErrorNotFound).
		AnyTimes()

	h := NewHandler(d, logger)

	r := mux.NewRouter()
	r.HandleFunc("/geodata/{ipv4}", h.GetGeodataFunc)

	tests := []struct {
		name         string
		url          string
		expectedCode int
		expectedBody string
	}{
		{
			name:         "Green path test",
			url:          "/geodata/127.0.0.1",
			expectedCode: 200,
			expectedBody: `{"ipv4":"127.0.0.1","countryCode":"RU","country":"Russia","city":"Dolgoprudny","latitude":123.456789,"longitude":-90.65332,"mysteryValue":123321}`,
		},
		{
			name:         "Non-existed ip",
			url:          "/geodata/216.71.194.58",
			expectedCode: 404,
		},
		{
			name:         "Malformed ip",
			url:          "/geodata/hello_world",
			expectedCode: 400,
			expectedBody: `{"error":"wrong IPv4 format"}`,
		},
		{
			name:         "Ip with some internal error",
			url:          "/geodata/13.0.0.1",
			expectedCode: 500,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rr := httptest.NewRecorder()
			req, err := http.NewRequest("GET", tt.url, nil)
			if err != nil {
				t.Fatal(err)
			}
			r.ServeHTTP(rr, req)

			if status := rr.Code; status != tt.expectedCode {
				t.Errorf("handler returned wrong status code: got %v want %v", status, tt.expectedCode)
			}

			gotBody := rr.Body.String()
			if gotBody != tt.expectedBody {
				t.Errorf("differtent body, expected: %s, got %s", tt.expectedBody, gotBody)
			}
		})
	}
}
