package get_geodata

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net"
	"net/http"
	"test-findhotel/internal/geolocation"
	"test-findhotel/internal/geolocation/storage"
)

const pathIpv4Param = "ipv4"

type handler struct {
	log  logrus.FieldLogger
	data geolocation.DataServer
}

func NewHandler(d geolocation.DataServer, l logrus.FieldLogger) *handler {
	return &handler{
		data: d,
		log:  l,
	}
}

func (h *handler) GetGeodataFunc(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ipv4 := vars[pathIpv4Param]
	if net.ParseIP(ipv4) == nil {
		h.badRequestError(w, "wrong IPv4 format")
		return
	}

	data, err := h.data.GetByIpv4(ipv4)
	if errors.Is(err, storage.ErrorNotFound) {
		h.notFoundError(w, ipv4)
		return
	}
	if err != nil {
		h.internalServerError(w, err)
		return
	}

	b, err := json.Marshal(data)
	if err != nil {
		h.internalServerError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	_, err = w.Write(b)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	return
}

func (h *handler) badRequestError(w http.ResponseWriter, message string) {
	b, err := json.Marshal(map[string]string{
		"error": message,
	})
	if err != nil {
		h.internalServerError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	_, err = w.Write(b)
	if err != nil {
		h.internalServerError(w, err)
		return
	}
}

func (h *handler) internalServerError(w http.ResponseWriter, err error) {
	h.log.Warning("failed to encode json: " + err.Error())
	w.WriteHeader(http.StatusInternalServerError)
}

func (h *handler) notFoundError(w http.ResponseWriter, ipv4 string) {
	h.log.Info("not found route by ipv4: " + ipv4)
	w.WriteHeader(http.StatusNotFound)
}
