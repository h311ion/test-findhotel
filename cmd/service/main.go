package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"test-findhotel/internal/geolocation/service"
	"test-findhotel/internal/geolocation/storage"
	"test-findhotel/internal/handlers/get_geodata"
	"time"
)

func main() {
	var log = logrus.New()

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	filePath := os.Getenv("FILE_PATH")
	file, err := os.Open(filePath)
	if err != nil {
		log.Fatal("failed to open file: " + err.Error())
	}

	s := service.New(storage.NewMemoryStorage(nil))

	log.Infof("start building data from file '%s'", filePath)
	start := time.Now()
	resp, err := s.Load(file)
	timeTaken := time.Since(start)
	if err != nil {
		log.Fatal("failed to build geo data: " + err.Error())
	}
	log.Infof("loaded %d geo entities, %d failed in %f seconds\n", resp.NumberOfOk, resp.NumberOfErrors, timeTaken.Seconds())

	r := mux.NewRouter()
	r.HandleFunc("/geodata/{ipv4}", get_geodata.NewHandler(s, logrus.New()).GetGeodataFunc)

	listenPath := fmt.Sprintf("%s:%s", os.Getenv("APP_HOST"), os.Getenv("APP_PORT"))
	log.Info("serving on " + listenPath)
	err = http.ListenAndServe(listenPath, r)
	if err != nil {
		log.Fatal("failed to serve: " + err.Error())
	}
}
