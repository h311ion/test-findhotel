FROM golang:1.17-alpine

WORKDIR /app

COPY . .

RUN go build -o ./bin/main ./cmd/service

EXPOSE ${APP_PORT}

CMD ["./bin/main"]