module test-findhotel

go 1.17

//github.com/deepmap/oapi-codegen v1.11.0
//github.com/getkin/kin-openapi v0.98.0
require github.com/gorilla/mux v1.8.0

require (
	github.com/golang/mock v1.6.0
	github.com/joho/godotenv v1.4.0
	github.com/sirupsen/logrus v1.9.0
	go.octolab.org v0.12.1
)

require (
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	golang.org/x/tools v0.1.1 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
