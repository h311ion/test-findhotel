.PHONY: tools test coverage run
all: help

help:
	@echo "run            - runs application on default port 8081 (default)"
	@echo "test           - run tests"
	@echo "coverage       - run tests with coverage"
	@echo "tools          - build development tools"

run:
	docker-compose up

tools:
	$(info Installing tools...)
	@cd tools && go generate -tags tools
	$(info Done)

test:
	docker run --rm -w /app -v \
		$(PWD):/app golang:1.17 \
		go test ./...

coverage:
	docker run --rm -w /app -v $(PWD):/app golang:1.17 go test ./... -coverprofile=/tmp/count.out fmt && go tool cover -html=/tmp/count.out
