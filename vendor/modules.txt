# github.com/golang/mock v1.6.0
## explicit; go 1.11
github.com/golang/mock/gomock
github.com/golang/mock/mockgen
github.com/golang/mock/mockgen/model
# github.com/gorilla/mux v1.8.0
## explicit; go 1.12
github.com/gorilla/mux
# github.com/joho/godotenv v1.4.0
## explicit; go 1.12
github.com/joho/godotenv
# github.com/sirupsen/logrus v1.9.0
## explicit; go 1.13
github.com/sirupsen/logrus
github.com/sirupsen/logrus/hooks/test
# go.octolab.org v0.12.1
## explicit; go 1.13
go.octolab.org/pointer
# golang.org/x/mod v0.4.2
## explicit; go 1.12
golang.org/x/mod/internal/lazyregexp
golang.org/x/mod/modfile
golang.org/x/mod/module
golang.org/x/mod/semver
# golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8
## explicit; go 1.17
golang.org/x/sys/execabs
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/tools v0.1.1
## explicit; go 1.17
golang.org/x/tools/go/ast/astutil
golang.org/x/tools/imports
golang.org/x/tools/internal/event
golang.org/x/tools/internal/event/core
golang.org/x/tools/internal/event/keys
golang.org/x/tools/internal/event/label
golang.org/x/tools/internal/fastwalk
golang.org/x/tools/internal/gocommand
golang.org/x/tools/internal/gopathwalk
golang.org/x/tools/internal/imports
# golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
## explicit; go 1.11
golang.org/x/xerrors
golang.org/x/xerrors/internal
