//go:build tools

//go:generate go build -o ../bin/mockgen github.com/golang/mock/mockgen/model

package tools

import _ "github.com/golang/mock/mockgen"
import _ "github.com/golang/mock/mockgen/model"
